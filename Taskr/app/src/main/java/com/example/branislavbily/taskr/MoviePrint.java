package com.example.branislavbily.taskr;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MoviePrint extends AppCompatActivity {

    public static final String TASK_BUNDLE_KEY = "task";
    public static final String TASK_ID_EXTRA = "taskId";
    private EditText filmNameEditText;
    private EditText filmGenreEditText;
    private EditText filmRatingEditText;
    private MovieList zoznamFilmov = MovieList.INSTANCE;
    private MovieDetails detailyFilmu;
    private boolean ignoreSaveOnFinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_print);
        filmNameEditText = findViewById(R.id.filmNameEditText);
        filmGenreEditText =  findViewById(R.id.filmGenreEditText);
        filmRatingEditText = findViewById(R.id.filmRatingEditText);
        if(savedInstanceState != null) {
            detailyFilmu = (MovieDetails) savedInstanceState.get(TASK_BUNDLE_KEY);
        } else {
            Long filmId = (Long) getIntent().getSerializableExtra(TASK_ID_EXTRA);
            if(filmId != null) {
                detailyFilmu = zoznamFilmov.getMovieDetails(filmId);
            } else {
                detailyFilmu = new MovieDetails();
            }
        }
        filmNameEditText.setText(detailyFilmu.getName());
        filmGenreEditText.setText(detailyFilmu.getGenre());
        filmRatingEditText.setText(Double.toString(detailyFilmu.getRating()));
    }
    private void saveFilm() {
        if(ignoreSaveOnFinish) {
            ignoreSaveOnFinish = false;
            return;
        }
        detailyFilmu.setName(filmNameEditText.getText().toString());
        detailyFilmu.setName(filmGenreEditText.getText().toString());
        detailyFilmu.setName(filmRatingEditText.getText().toString());
        zoznamFilmov.saveOrUpdate(detailyFilmu);
        Log.d(getClass().getName(), "Task " + detailyFilmu + " was saved");
    }
    @Override
    protected void onPause() {
        super.onPause();
        saveFilm();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveFilm();
        outState.putSerializable(TASK_BUNDLE_KEY, detailyFilmu);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_detail, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.deleteTaskMenu) {
            zoznamFilmov.delete(detailyFilmu);
            ignoreSaveOnFinish = true;
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
