package com.example.branislavbily.taskr;

import java.io.Serializable;

public class MovieDetails implements Serializable {
        private Long id;
        private String name;
        private String genre;
        private double rating;

        public MovieDetails() {

        }

        public MovieDetails(String name, String genre, double rating) {
            this.name = name;
            this.genre = genre;
            this.rating = rating;
        }

        public MovieDetails(String name) {
            this(name,"",0.0);
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getGenre() {
            return genre;
        }

        public double getRating() {
            return rating;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setGenre(String genre) {
            this.genre = genre;
        }

        public void setRating(double rating) {
            this.rating = rating;
        }
}
