package com.example.branislavbily.taskr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import static com.example.branislavbily.taskr.MoviePrint.TASK_ID_EXTRA;

public class MainActivity extends AppCompatActivity {

    private ListView filmListView;
    private MovieList zoznamFilmov = MovieList.INSTANCE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        filmListView = findViewById(R.id.filmListView);
        filmListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MovieDetails film = zoznamFilmov.list().get(position);
                Intent intent = new Intent(MainActivity.this, MoviePrint.class);
                intent.putExtra(TASK_ID_EXTRA, film.getId());
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        ArrayAdapter<MovieDetails> taskAdapter = new ArrayAdapter<MovieDetails>(this, android.R.layout.simple_list_item_1, zoznamFilmov.list()) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView listItemView = (TextView) super.getView(position, convertView, parent);
                MovieDetails film = getItem(position);
                listItemView.setText(film.getName());
                return listItemView;
            }
        };
        filmListView.setAdapter(taskAdapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_list, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.addNewTaskMenu) {
            Intent intent = new Intent(this, MoviePrint.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
