package com.example.branislavbily.taskr;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public enum  MovieList {
    INSTANCE;
    private List<MovieDetails> filmy = new LinkedList<>();
    private long idGenerator = 0;

    MovieList() {
        MovieDetails pirates_of_caribean = new MovieDetails("Pirates of Caribean", "Fantasy",9.1);
        saveOrUpdate(pirates_of_caribean);
        MovieDetails ironMan = new MovieDetails("Iron Man", "Sci-fi", 8.2);
        saveOrUpdate(ironMan);
        MovieDetails venom = new MovieDetails("Venom", "Sci-fi", 7.1);
        saveOrUpdate(venom);
    }
    public void saveOrUpdate(MovieDetails film) {
        if (film.getId() == null) {
            film.setId(idGenerator++);
            filmy.add(film);
        } else {
            Iterator<MovieDetails> iterator = filmy.iterator();
            int index = 0;
            while (iterator.hasNext()) {
                MovieDetails t = iterator.next();
                if (t.getId().equals(film.getId())) {
                    iterator.remove();
                    break;
                }
                index++;
            }
            filmy.add(index, film);
        }
    }
    public List<MovieDetails> list() {
        return new LinkedList<>(this.filmy);
    }
    public MovieDetails getMovieDetails(long filmId) {
        for (MovieDetails film : this.filmy) {
            if (film.getId() == filmId) {
                return film;
            }
        }
        return null;
    }
    public void delete(MovieDetails film) {
        Iterator<MovieDetails> iterator = this.filmy.iterator();
        while(iterator.hasNext()) {
            MovieDetails t = iterator.next();
            if(t.getId() == film.getId()) {
                iterator.remove();
            }
        }
    }
}
