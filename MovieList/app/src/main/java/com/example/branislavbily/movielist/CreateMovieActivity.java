package com.example.branislavbily.movielist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;

import static com.example.branislavbily.movielist.MainActivity.TASK_BUNDLE_KEY;

public class CreateMovieActivity extends AppCompatActivity {

    private EditText movieName;
    private EditText movieGenre;
    private EditText movieRating;

    private String name;
    private String genre;
    private String ratingS;
    private double rating;
    private boolean ignoreSaveOnFinish;

    private Movie movie;

    private MovieLibrary library;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_movie);
        library = MovieLibrary.INSTANCE;
    }

    public void onCreateButtonClicked(View view) {
        movieName = findViewById(R.id.editTextName);
        movieGenre = findViewById(R.id.editTextGenre);
        movieRating = findViewById(R.id.editTextRating);
        movie = new Movie();

        name = movieName.getText().toString();
        genre = movieGenre.getText().toString();
        ratingS = movieRating.getText().toString();
        if(name.equals("") || genre.equals("") || ratingS.equals("")) {
            Toast.makeText(this, "You need to enter all values!", Toast.LENGTH_SHORT).show();
        } else {
            rating = Double.parseDouble(ratingS);
            Movie movie = new Movie(name, genre, rating);
            library.addMovie(movie);
            finish();
        }
    }

    private void saveFilm() {
        if(ignoreSaveOnFinish) {
            ignoreSaveOnFinish = false;
            return;
        }
        movie.setName(movieName.getText().toString());
        movie.setName(movieGenre.getText().toString());
        movie.setName(movieRating.getText().toString());
        library.saveOrUpdate(movie);
        Log.d(getClass().getName(), "Task " + movie + " was saved");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveFilm();
        outState.putSerializable(TASK_BUNDLE_KEY, (Serializable) movie);
    }
}
