package com.example.branislavbily.movielist;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.branislavbily.movielist.MainActivity.TASK_ID_EXTRA;

public class MovieDetailsActivity extends AppCompatActivity {

    //TODO Rename buttons to make more sense, fix alignment of the buttons, maybe implement the database

    private MovieLibrary library = MovieLibrary.INSTANCE;
    private ArrayList<Movie> movies;
    private Movie movie;
    private TextView name;
    private TextView genre;
    private TextView rating;

    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        movies = library.getMovies();

        Intent newInt = getIntent();
        id =  (Integer) newInt.getSerializableExtra(TASK_ID_EXTRA);
        movie = movies.get(id);

        name = findViewById(R.id.name);
        genre = findViewById(R.id.genre);
        rating = findViewById(R.id.rating);

        name.setText(movie.getName());
        genre.setText(movie.getGenre());
        rating.setText(Double.toString(movie.getRating()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_movie_details_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.removeTaskMenu) {
            alertDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    public void alertDialog() {
        final AlertDialog alertDialog = createAlertDialog();

        TextView title = createTitle();
        alertDialog.setCustomTitle(title);

        TextView msg = createMessage();
        alertDialog.setView(msg);

        setNeutralButton(alertDialog);
        setNegativeButton(alertDialog);

        new Dialog(getApplicationContext());
        alertDialog.show();

        final Button okBT = createButtonOk(alertDialog);

        createButtonNegative(alertDialog, okBT);
    }

    private AlertDialog createAlertDialog() {
        return new AlertDialog.Builder(this).create();
    }

    private void setNeutralButton(final AlertDialog alertDialog) {
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,"NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            }
        });
    }

    private void setNegativeButton(AlertDialog alertDialog) {
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                library.removeMovie(movies.get(id));
                finish();
            }
        });
    }

    private TextView createTitle() {
        TextView title = new TextView(this);
        title.setText(R.string.alert_dialog_title);
        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        title.setTextSize(25);
        return title;
    }

    private TextView createMessage() {
        TextView msg = new TextView(this);
        msg.setText(R.string.alert_dialog_message);
        msg.setGravity(Gravity.CENTER_HORIZONTAL);
        msg.setTextColor(Color.BLACK);
        msg.setTextSize(20);
        return msg;
    }

    private Button createButtonOk(AlertDialog alertDialog) {
        Button okBT = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
        LinearLayout.LayoutParams neutralBtnLP = (LinearLayout.LayoutParams) okBT.getLayoutParams();
        okBT.setBackgroundResource(R.drawable.button_alert_dialog);
        neutralBtnLP.gravity = Gravity.FILL_HORIZONTAL;
        okBT.setTextColor(Color.WHITE);
        okBT.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        okBT.setTextSize(20);
        okBT.setLayoutParams(neutralBtnLP);
        return okBT;
    }

    private void createButtonNegative(AlertDialog alertDialog, Button okBT) {
        Button cancelBT = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams negBtnLP = (LinearLayout.LayoutParams) okBT.getLayoutParams();
        cancelBT.setBackgroundResource(R.drawable.button_red_alert_dialog);
        negBtnLP.gravity = Gravity.FILL_HORIZONTAL;
        cancelBT.setTextColor(Color.WHITE);
        cancelBT.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        cancelBT.setTextSize(20);
        cancelBT.setLayoutParams(negBtnLP);
    }


}
