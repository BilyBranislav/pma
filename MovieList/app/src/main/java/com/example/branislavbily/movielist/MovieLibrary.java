package com.example.branislavbily.movielist;

import java.util.ArrayList;
import java.util.Iterator;

public enum MovieLibrary {
    INSTANCE;
    private ArrayList<Movie> movies = new ArrayList<>();
    private long idGenerator = 0;

    MovieLibrary() {

        Movie Hej = new Movie("Bohemian Rhapsody", "Biography", 5.9);
        movies.add(Hej);

        Movie What = new Movie("Good Dinosaur", "Fantasy", 6.7);
        movies.add(What);

        Movie Meh = new Movie("Nun", "Horror", 5);
        movies.add(Meh);
    }

    public ArrayList<Movie> getMovies() {
        return movies;
    }

    public void saveOrUpdate(Movie movie) {
        if (movie.getId() == null) {
            movie.setId(idGenerator++);
            movies.add(movie);
        } else {
            Iterator<Movie> iterator = movies.iterator();
            int index = 0;
            while (iterator.hasNext()) {
                Movie t = iterator.next();
                if (t.getId().equals(movie.getId())) {
                    iterator.remove();
                    break;
                }
                index++;
            }
            movies.add(index, movie);
        }
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public void addMovie(Movie movie) {
        movies.add(movie);
    }

    public void removeMovie(Movie movie) {
        movies.remove(movie);
    }
}
