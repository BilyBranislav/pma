package com.example.branislavbily.movielist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private ListView listView;
    private MovieLibrary library;
    public static final String TASK_BUNDLE_KEY = "task";
    public static final String TASK_ID_EXTRA = "taskId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);

        library = MovieLibrary.INSTANCE;
        ArrayList<Movie> movies = library.getMovies();

        ArrayAdapter<Movie> moviesAdapter = createArrayAdapter(movies);
        setArrayAdapter(moviesAdapter, listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, MovieDetailsActivity.class);
                intent.putExtra(TASK_ID_EXTRA, position);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ArrayAdapter<Movie> moviesAdapter = createArrayAdapter(library.getMovies());
        setArrayAdapter(moviesAdapter, listView);
    }

    private ArrayAdapter<Movie> createArrayAdapter(ArrayList<Movie> arrayList) {
        return new ArrayAdapter<Movie>(this, android.R.layout.simple_list_item_1, arrayList) {
            @Override
            public View getView(int position, View convertView,ViewGroup parent) {
                TextView listItemView = (TextView) super.getView(position, convertView, parent);
                Movie movie = getItem(position);
                listItemView.setText(movie.getName());
                return listItemView;
            }
        };
    }

    private void setArrayAdapter(ArrayAdapter arrayAdapter, ListView listView) {
        listView.setAdapter(arrayAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.addNewTaskMenu) {
            Intent intent = new Intent(this, CreateMovieActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
