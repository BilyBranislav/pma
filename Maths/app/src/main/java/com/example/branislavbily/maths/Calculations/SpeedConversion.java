package com.example.branislavbily.maths.calculations;

public class SpeedConversion {

    public double calculateKilometres(double metres) {
        return metres * 3.6;
    }

    public double calculateMetres(double kilometres) {
        if(kilometres == 0) {
            return 0;
        } else {
            return (double) Math.round(kilometres / 3.6 * 100) / 100;
        }
    }
}
