package com.example.branislavbily.maths.calculations;


public class QuadraticEquation {

    private double a, b, c, d;
    private double[] roots;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;

        d = Math.pow(b, 2) - 4 * a * c;

        if (d > 0) {
            double squareRoot = Math.sqrt(d);
            roots = new double[2];
            roots[0] = (double) Math.round((-b + squareRoot) / (2 * a) * 100) / 100;
            roots[1] = (double) Math.round((-b - squareRoot) / (2 * a) * 100) / 100;
        } else if (d == 0) {
            roots = new double[1];
            roots[0] =(double) Math.round(-b / (2 * a) * 100) / 100;
        } else {
            roots = null;
        }
    }

    public double[] getRoots() {
        return roots;
    }

    public double getDiscriminant() {
        return d;

    }


}
