package com.example.branislavbily.maths.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.branislavbily.maths.calculations.BytesConversion;
import com.example.branislavbily.maths.R;

public class BytesFragment extends Fragment {

    private EditText editTextBytes, editTextKiloBytes;
    BytesConversion calculation;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bytes, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View v = getView();
        editTextBytes = v.findViewById(R.id.editTextByte);
        editTextKiloBytes = v.findViewById(R.id.editTextKiloByte);

        calculation = new BytesConversion();

        editTextBytes.addTextChangedListener(textWatcherByte);
        editTextKiloBytes.addTextChangedListener(textWatcherKiloByte);
    }



    private TextWatcher textWatcherByte = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(editTextBytes.isFocused()) {
                    try {
                        double bytes = Long.parseLong(s.toString());
                        double kilobytes = calculation.calculateKiloBytes(bytes);
                        String kB = Double.toString(kilobytes);
                        editTextKiloBytes.setText(kB);
                    } catch (NumberFormatException e) {
                        editTextKiloBytes.setText("0");
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };


    private TextWatcher textWatcherKiloByte = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(editTextKiloBytes.isFocused()) {
                try {
                    double kilobytes = Long.parseLong(s.toString());
                    double bytes = calculation.calculateBytes(kilobytes);
                    String B = Double.toString(bytes);
                    editTextBytes.setText(B);
                } catch (NumberFormatException e) {
                    editTextBytes.setText("0");
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
