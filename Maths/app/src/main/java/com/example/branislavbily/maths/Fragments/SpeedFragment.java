package com.example.branislavbily.maths.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.branislavbily.maths.calculations.SpeedConversion;
import com.example.branislavbily.maths.R;

public class SpeedFragment extends Fragment {

    private EditText editTextMetres, editTextKilometres;
    View v;
    SpeedConversion conversion;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_speed, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        v = getView();
        editTextMetres = v.findViewById(R.id.editTextMetresPerSecond);
        editTextKilometres = v.findViewById(R.id.editTextKilometresPerHour);

        conversion = new SpeedConversion();

        editTextMetres.addTextChangedListener(textWatcherMetres);
        editTextKilometres.addTextChangedListener(textWatcherKiloMetres);
    }

    private TextWatcher textWatcherMetres = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(editTextMetres.isFocused()) {
                    try {
                        double metres = Double.parseDouble(s.toString());
                        double kilometres = conversion.calculateKilometres(metres);
                        String km = Double.toString(kilometres);
                        editTextKilometres.setText(km);
                    } catch (NumberFormatException e) {
                        editTextKilometres.setText("0");
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

    private TextWatcher textWatcherKiloMetres = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(editTextKilometres.isFocused()) {
                try {
                    double kilometres = Double.parseDouble(s.toString());
                    double metres = conversion.calculateMetres(kilometres);
                    String ms = Double.toString(metres);
                    editTextMetres.setText(ms);
                } catch (NumberFormatException e) {
                    editTextMetres.setText("0");
                }

            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
