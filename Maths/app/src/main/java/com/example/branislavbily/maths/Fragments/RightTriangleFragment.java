package com.example.branislavbily.maths.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.branislavbily.maths.calculations.RightTriangle;
import com.example.branislavbily.maths.R;

public class RightTriangleFragment extends Fragment {

    private EditText editTextAdjacentSide, editTextOppositeSide, editTextHypotenuse;
    private Button buttonCalculate;
    private RightTriangle calculator;
    private View v;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_right_triangle, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        v = getView();

        editTextAdjacentSide = v.findViewById(R.id.editTextStranaA);
        editTextOppositeSide = v.findViewById(R.id.editTextStranaB);
        editTextHypotenuse = v.findViewById(R.id.editTextPrepona);

        buttonCalculate = v.findViewById(R.id.button);

        calculator = new RightTriangle();

        buttonCalculate.setOnClickListener(view -> {
            double adjacentSide, oppositeSide, hypotenuse;
            adjacentSide = getAdjacentSide();
            oppositeSide = getOppositeSide();
            hypotenuse = getHypotenuse();
            //If neither side is 0
            if(adjacentSide * oppositeSide * hypotenuse != 0) {
                if(calculator.isRight(adjacentSide, oppositeSide, hypotenuse)) {
                    Toast.makeText(getActivity(), "This is right triangle", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "This is not a right triangle", Toast.LENGTH_SHORT).show();
                }
            } else if(oppositeSide == 0 && adjacentSide == 0 && hypotenuse == 0) {
                Toast.makeText(getActivity(), R.string.not_enough_values_entered, Toast.LENGTH_SHORT).show();
            } else if(oppositeSide == 0) {
                double result = calculator.calculateSide(adjacentSide, hypotenuse);
                String res = Double.toString(result);
                editTextOppositeSide.setText(res);
            } else if(adjacentSide == 0) {
                double result = calculator.calculateSide(oppositeSide, hypotenuse);
                String res = Double.toString(result);
                editTextAdjacentSide.setText(res);
            } else {
                double result = calculator.calculateHypotenuse(adjacentSide, oppositeSide);
                String res = Double.toString(result);
                editTextHypotenuse.setText(res);
            }
        });
    }
    
    private Double getAdjacentSide() {
        try {
            return Double.parseDouble(editTextAdjacentSide.getText().toString());
        } catch(NumberFormatException e) {
            return 0.0;
        }
    }

    private Double getOppositeSide() {
       try {
           return Double.parseDouble(editTextOppositeSide.getText().toString());
       } catch (NumberFormatException e) {
           return 0.0;
       }
    }

    private Double getHypotenuse() {
        try {
            return Double.parseDouble(editTextHypotenuse.getText().toString());
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }
}
