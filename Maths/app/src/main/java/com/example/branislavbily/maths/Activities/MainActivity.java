package com.example.branislavbily.maths.activities;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.branislavbily.maths.fragments.BytesFragment;
import com.example.branislavbily.maths.fragments.QuadraticFragment;
import com.example.branislavbily.maths.fragments.RightTriangleFragment;
import com.example.branislavbily.maths.fragments.SpeedFragment;
import com.example.branislavbily.maths.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);

        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_layout, new QuadraticFragment()).commit();

    }
    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
            Fragment selectedFragment = null;
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    switch (menuItem.getItemId()) {
                        case R.id.nav_power:
                            selectedFragment = new QuadraticFragment();
                            break;
                        case R.id.nav_speed:
                            selectedFragment = new SpeedFragment();
                            break;
                        case R.id.nav_bytes:
                            selectedFragment = new BytesFragment();
                            break;
                        case R.id.nav_triangle:
                            selectedFragment = new RightTriangleFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_layout, selectedFragment).commit();
                    return true;
                }
            };
}
