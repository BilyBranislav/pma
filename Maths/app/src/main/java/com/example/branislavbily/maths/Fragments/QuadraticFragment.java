package com.example.branislavbily.maths.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.branislavbily.maths.calculations.QuadraticEquation;
import com.example.branislavbily.maths.R;

public class QuadraticFragment extends Fragment {

    private EditText editTextA, editTextB, editTextC;
    private View v;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_quadratic, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        v = getView();

        editTextA = v.findViewById(R.id.editTextElementA);
        editTextB = v.findViewById(R.id.editTextElementB);
        editTextC = v.findViewById(R.id.editTextElementC);

        TextView textViewA = v.findViewById(R.id.textViewElementA);
        textViewA.setText(Html.fromHtml("x<sup>2</sup>"));

        Button calculateButton = v.findViewById(R.id.calculateButton);
        calculateButton.setOnClickListener(v -> onCalculateButtonPressed());
    }

    private void onCalculateButtonPressed() {
        TextView textViewP = v.findViewById(R.id.textViewP);
        TextView textViewD = v.findViewById(R.id.textViewD);
        textViewP.setText("P =");
        textViewD.setText("D =");
        double a, b, c;
        try {
            a = Double.parseDouble(editTextA.getText().toString());
            b = Double.parseDouble(editTextB.getText().toString());
            c = Double.parseDouble(editTextC.getText().toString());

            QuadraticEquation equation = new QuadraticEquation(a, b, c);
            double[] roots = equation.getRoots();
            double d = equation.getDiscriminant();
            textViewD.append(Double.toString(d));
            textViewP.append("{");
            if(roots == null) {
                Toast.makeText(getActivity(), R.string.equation_has_no_roots, Toast.LENGTH_SHORT).show();
            } else if( roots.length > 1) {
                textViewP.append(roots[0] + "; " + roots[1]);
            } else {
                textViewP.append(roots[0] + "");
            }

            textViewP.append("}");
        } catch (NumberFormatException e ){
            Toast.makeText(getActivity(), R.string.not_enough_values_entered, Toast.LENGTH_SHORT).show();
        }
    }
}
