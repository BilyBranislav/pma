package com.example.branislavbily.maths.calculations;

public class RightTriangle {

    public double calculateSide(double side, double hypotenuse) {
        return (double) Math.round(Math.sqrt(Math.pow(hypotenuse, 2) - Math.pow(side, 2)) * 100) / 100;
    }

    public double calculateHypotenuse(double adjacentSide, double oppositeSide) {
        return (double) Math.round(Math.sqrt(Math.pow(adjacentSide, 2) + Math.pow(oppositeSide, 2)) * 100) / 100;
    }

    public boolean isRight(double adjacentSide, double oppositeSide, double hypotenuse) {
        return Math.pow(adjacentSide, 2) + Math.pow(oppositeSide, 2) == Math.pow(hypotenuse, 2);
    }
}
