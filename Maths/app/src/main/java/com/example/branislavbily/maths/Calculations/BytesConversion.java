package com.example.branislavbily.maths.calculations;

public class BytesConversion {

    public double calculateBytes(double kilobytes) {
        return kilobytes * 1000;
    }

    public double calculateKiloBytes(double bytes) {
        if(bytes == 0) {
            return 0;
        } else {
            return bytes / 1000;
        }
    }
}
