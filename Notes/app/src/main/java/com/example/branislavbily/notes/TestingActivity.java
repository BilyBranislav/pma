package com.example.branislavbily.notes;

import android.database.Cursor;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

public class TestingActivity extends AppCompatActivity {

    DatabaseHelper myDB;
    EditText editTextDescription;
    EditText editTextID;
    EditText editTextUpdateDescription;
    EditText editTextTimestamp;
    EditText editTextDeleteID;
    EditText editTextTitle;
    EditText editTextTitleUpdate;
    Button buttonInsertData;
    Button buttonViewAll;
    Button buttonUpdate;
    Button buttonDelete;

    String id, description;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .build());

        editTextDescription = findViewById(R.id.editText);
        editTextID = findViewById(R.id.editText2);
        editTextUpdateDescription = findViewById(R.id.editText3);
        editTextTimestamp = findViewById(R.id.editText4);
        editTextDeleteID = findViewById(R.id.editText5);
        editTextTitle = findViewById(R.id.editText6);
        editTextTitleUpdate = findViewById(R.id.editText7);

        buttonInsertData = findViewById(R.id.button);
        buttonViewAll = findViewById(R.id.button2);
        buttonUpdate = findViewById(R.id.button3);
        buttonDelete = findViewById(R.id.button4);

        buttonInsertData.setOnClickListener(view -> {
            String description = editTextDescription.getText().toString();
            String title = editTextTitle.getText().toString();
            if(!(description.equals("")) && !(title.equals(""))) {
                if(myDB.insertData(title,description)) {
                    Toast.makeText(this, "Data inserted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Data not inserted", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Please enter your note!", Toast.LENGTH_SHORT).show();
            }
        });

        buttonViewAll.setOnClickListener(view -> {
            Cursor result = myDB.getAllData();
            if(result.getCount() == 0) {
                showMessage("Error", "No available data");
                return;
            }
            StringBuffer buffer = new StringBuffer();
            while(result.moveToNext()) {
                buffer.append("ID: ").append(result.getString(0) + "\n")
                      .append("TITLE: ").append(result.getString(1) + "\n")
                      .append("DESCRIPTION: ").append(result.getString(2) + "\n")
                      .append("DATE: ").append(result.getString(3) + "\n\n");
            }
            showMessage("Success", buffer.toString());
        });

        buttonUpdate.setOnClickListener(view -> {
            String id = editTextID.getText().toString();
            String title = editTextTitleUpdate.getText().toString();
            String description = editTextUpdateDescription.getText().toString();
            String timeStamp = editTextTimestamp.getText().toString();
            if(myDB.updateData(id,title, description, timeStamp)) {
                showMessage("Success", "Data updated successfully");
            } else {
                showMessage("Error", "Could not update data");
            }
        });

        buttonDelete.setOnClickListener(view -> {
            String id = editTextDeleteID.getText().toString();
            int rowsDeleted = myDB.deleteData(id);
            Toast.makeText(this,rowsDeleted + " rows were affected.", Toast.LENGTH_SHORT).show();
        });

        myDB = new DatabaseHelper(this);

    }

    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
}
