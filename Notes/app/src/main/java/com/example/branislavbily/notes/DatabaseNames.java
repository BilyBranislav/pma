package com.example.branislavbily.notes;

import android.provider.BaseColumns;

interface Provider {
     interface Note extends BaseColumns {

        String DATABASE_NAME = "Notes.db";

        String TABLE_NAME = "notes_table";

        String TITLE = "title";

        String DESCRIPTION = "description";

        String DATETIME = "createdOn";
    }
}