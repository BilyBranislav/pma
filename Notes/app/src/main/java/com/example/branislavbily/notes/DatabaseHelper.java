package com.example.branislavbily.notes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.time.LocalDate;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, Provider.Note.DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table IF EXISTS " + Provider.Note.TABLE_NAME);
        onCreate(db);
    }

    public String createTableQuery() {
        String sqlTemplate = "CREATE TABLE %s ("
                + "%s INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "%s TEXT,"
                + "%s TEXT,"
                + "%s DATETIME"
                + ")";
        return String.format(sqlTemplate, Provider.Note.TABLE_NAME, "ID",Provider.Note.TITLE, Provider.Note.DESCRIPTION, Provider.Note.DATETIME);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean insertData(String title, String description) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Provider.Note.TITLE, title);
        contentValues.put(Provider.Note.DESCRIPTION, description);
        contentValues.put(Provider.Note.DATETIME, LocalDate.now().toString());
        long result = db.insert(Provider.Note.TABLE_NAME, null, contentValues);
        if(result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor result = db.rawQuery(selectAllFromQuery(), null);
        return result;
    }

    public String selectAllFromQuery() {
        return "SELECT * FROM " + Provider.Note.TABLE_NAME;
    }

    public boolean updateData(String id,String title, String description, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ID", id);
        contentValues.put(Provider.Note.TITLE, title);
        contentValues.put(Provider.Note.DESCRIPTION, description);
        contentValues.put(Provider.Note.DATETIME, date);
        int result = db.update(Provider.Note.TABLE_NAME, contentValues, "id = ?", new String[] {id});
        if(result == 0) {
            return  false;
        } else {
            return true;
        }
    }

    public int deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(Provider.Note.TABLE_NAME, "id = ?", new String[] {id});

    }
}